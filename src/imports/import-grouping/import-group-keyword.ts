/**
 * Enum for the different special keywords of the KeywordImportGroup.
 *
 * @export
 * @enum {number}
 */
export enum ImportGroupKeyword {
  Modules = 'Modules',
  Plains = 'Plains',
  Workspace = 'Workspace',
  Remaining = 'Remaining',
}
